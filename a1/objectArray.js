/*We can also have an array of objects. We can group together object in an array.
That array can also use array methids for the objects. However, being objects are more complex than strings or numbers, there is a difefrence when handling them.

*/
// alert ('hi')
let users = [
	{
		name: "Mike Shell",
		username: "mikeBlueShell",
		email: "mikeShell01@gmail.com",
		password: "iamikey1999",
		isActive: true,
		dateJoined: "August 8, 2011"
	},
	{
		name: "Jake Janella",
		username: "jjnella99",
		email: "jakejanella_99@gmail.com",
		password:"jackiejackie2",
		isActive: true,
		dateJoined: "January 14, 2015"
	}
];

console.log(users[0]);
// How can we access the properties of our object in an array?
// show the secon items' email propert
console.log(users[1].email);

// Can we update the properties of objects in an array?
/*
Update the emails of both users:
user 1 ="mikeShell01@gmail.com"
user 2 = "jakejanella_99@gmail.com"

*/



users [0].email = "mikeKingOfShells@gmail.com"
users [1].email = "janellajakeArchitect@gmail.com"

console.log(users);

// Can we also use array methods for array of objects?
// Add another user into the array: push

users.push({
		name: "James Jameson",
		username: "iHateSpidey",
		email: "jamesJjameson@gmail.com",
		password: "spideyisamenace64",
		isActive: true,
		dateJoined: "February 14, 2021"

});

console.log(users);

// this keyword refers to the object it belong

class User {
	constructor (name,username, email, password){
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.isActive = true;
		this.dateJoined = "September 28, 2021";
	}
}

let newUser1 = new User ("Kate Middleton", "notTheDuchess", "serouslyNotDuchess@gmail.com", "notRoyaltyAtAll");
console.log(newUser1);

console.log(users);

//push
users.push(newUser1);

console.log(users);

// find()- is able to return the fund item

// function login(username, password){

// // check if the username does not exist or any uses that username
// // check if the password given matches the password of our user in the array


// 	let userFound = users.find((user) => {
// 		// (user) is a parameter which receives each items in the users array and the users array is an array of objects. Therefore, we can expect that the user parameter will contain an object
// 		return user.username === username && user.password
		
// 	})
// 	console.log(userFound);
//  }
 

//  login("notTheDuchess", "notRoyaltyAtAll");
//  login("mikeBlueShell", "notRoyaltyAtAll");


 function login(username, password){


	let userFound = users.find((user) => {
		if (user.username === username && user.password === password)
		{
 		console.log("Thank you for Loggin in " + username)
 		}
 		else
 		{
 			console.log("Login failed. Invalid credentials.")
 		}
 		return (user.username === username && user.password === password)
 	})
 };
 

 login("notTheDuchess", "notRoyaltyAtAll");
 login("mikeBlueShell", "notRoyaltyAtAll");
